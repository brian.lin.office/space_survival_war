import random
from config import *


class Player(pygame.sprite.Sprite):
    def __init__(self, initial, all_sprites, bullets):
        pygame.sprite.Sprite.__init__(self)
        self.initial = initial
        # self.image = pygame.Surface((50, 40))
        # self.image.fill(GREEN)
        self.image = pygame.transform.scale(initial.player_img, (50, 38))
        self.image.set_colorkey(parameter.BLACK)  # 將黑色變透明
        self.rect = self.image.get_rect()
        self.radius = 20
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.centerx = parameter.WIDTH / 2
        self.rect.bottom = parameter.HEIGHT - 10
        self.speedx = 8
        self.health = 100
        self.lives = 3
        self.hidden = False
        self.hide_time = 0
        self.gun = 1
        self.gun_time = 0
        self.all_sprites = all_sprites
        self.bullets = bullets

    def update(self):
        now = pygame.time.get_ticks()
        if self.gun > 1 and now - self.gun_time > 5000:
            self.gun -= 1
            self.gun_time = now

        if self.hidden and now - self.hide_time > 1000:  # 1000毫秒=1秒
            self.hidden = False
            self.rect.centerx = parameter.WIDTH / 2
            self.rect.bottom = parameter.HEIGHT - 10

        key_pressed = pygame.key.get_pressed()  # 回傳布林值, 回傳鍵盤上每一個按鍵是否有被按下
        if key_pressed[pygame.K_RIGHT]:
            self.rect.x += self.speedx
        if key_pressed[pygame.K_LEFT]:
            self.rect.x -= self.speedx

        if self.rect.right > parameter.WIDTH:
            self.rect.right = parameter.WIDTH
        if self.rect.left < 0:
            self.rect.left = 0

    def shoot(self):
        if not self.hidden:
            if self.gun == 1:
                bullet = Bullet(self.rect.centerx, self.rect.top, self.initial)
                self.all_sprites.add(bullet)
                self.bullets.add(bullet)
                self.initial.shoot_sound.play()
            elif self.gun >= 2:
                bullet1 = Bullet(self.rect.left, self.rect.centery, self.initial)
                bullet2 = Bullet(self.rect.right, self.rect.centery, self.initial)
                self.all_sprites.add(bullet1)
                self.all_sprites.add(bullet2)
                self.bullets.add(bullet1)
                self.bullets.add(bullet2)
                self.initial.shoot_sound.play()

    def hide(self):
        self.hidden = True
        self.hide_time = pygame.time.get_ticks()
        self.rect.center = (parameter.WIDTH / 2, parameter.HEIGHT + 500)

    def gun_up(self):
        self.gun += 1
        self.gun_time = pygame.time.get_ticks()


class Rock(pygame.sprite.Sprite):
    def __init__(self, initial):
        pygame.sprite.Sprite.__init__(self)
        self.initial = initial
        # self.image = pygame.Surface((30, 40))
        # self.image.fill(RED)
        self.image_ori = random.choice(initial.rock_imgs)
        self.image_ori.set_colorkey(parameter.BLACK)
        self.image = self.image_ori.copy()
        self.rect = self.image.get_rect()
        self.radius = int(self.rect.width * 0.85 / 2)
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.x = random.randrange(0, parameter.WIDTH - self.rect.width)
        self.rect.y = random.randrange(-180, -100)
        self.speedx = random.randrange(-3, 3)
        self.speedy = random.randrange(2, 10)
        self.total_degree = 0  # 初始值為0度
        self.rot_degree = random.randrange(-3, 3)

    def rotate(self):
        self.total_degree += self.rot_degree
        self.total_degree = self.total_degree % 360
        self.image = pygame.transform.rotate(self.image_ori, self.total_degree)
        center = self.rect.center
        self.rect = self.image.get_rect()  # 對轉動後的圖,做重新定位
        self.rect.center = center  # 設定為原先的中心點, 中心點不變

    def update(self):
        self.rotate()
        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.top > parameter.HEIGHT or self.rect.left > parameter.WIDTH or self.rect.right < 0:
            self.rect.x = random.randrange(0, parameter.WIDTH - self.rect.width)
            self.rect.y = random.randrange(-100, -40)
            self.speedx = random.randrange(-3, 3)
            self.speedy = random.randrange(2, 10)


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, initial):
        pygame.sprite.Sprite.__init__(self)
        self.initial = initial
        # self.image = pygame.Surface((10, 20))
        # self.image.fill(YELLOW)
        self.image = initial.bullet_img
        self.image.set_colorkey(parameter.BLACK)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.bottom = y
        self.speedy = -10

    def update(self):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()


class Explosion(pygame.sprite.Sprite):
    def __init__(self, center, size, initial):
        pygame.sprite.Sprite.__init__(self)
        self.initial = initial
        self.size = size
        self.image = initial.expl_anim[self.size][0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0
        self.last_update = pygame.time.get_ticks()
        self.frame_rate = 50

    def update(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > self.frame_rate:
            self.last_update = now
            self.frame += 1
            if self.frame == len(self.initial.expl_anim[self.size]):
                self.kill()
            else:
                self.image = self.initial.expl_anim[self.size][self.frame]
                center = self.rect.center
                self.rect = self.image.get_rect()
                self.rect.center = center


class Power(pygame.sprite.Sprite):
    def __init__(self, center, initial):
        pygame.sprite.Sprite.__init__(self)
        self.type = random.choice(['shield', 'gun'])
        self.image = initial.power_imgs[self.type]
        self.image.set_colorkey(parameter.BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.speedy = 3

    def update(self):
        self.rect.y += self.speedy
        if self.rect.top > parameter.HEIGHT:
            self.kill()
