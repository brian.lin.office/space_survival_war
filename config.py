import pygame
import os
import parameter


class Config:
    def __init__(self):
        pygame.init()
        pygame.mixer.init()
        self.screen = pygame.display.set_mode((parameter.WIDTH, parameter.HEIGHT))
        pygame.display.set_caption(parameter.str_game_name)
        self.clock = pygame.time.Clock()

        # 載入圖片  # convert:將圖片轉換為pygame較容易讀取的格式
        self.background_img = pygame.image.load(os.path.join("img", "background.png")).convert()
        self.player_img = pygame.image.load(os.path.join("img", "player.png")).convert()
        self.player_mini_img = pygame.transform.scale(self.player_img, (25, 19))
        self.player_mini_img.set_colorkey(parameter.BLACK)
        pygame.display.set_icon(self.player_img)  # icon
        # self.rock_img = pygame.image.load(os.path.join("img", "rock.png")).convert()
        self.bullet_img = pygame.image.load(os.path.join("img", "bullet.png")).convert()
        self.rock_imgs = []
        for i in range(7):
            self.rock_imgs.append(pygame.image.load(os.path.join("img", f"rock{i}.png")).convert())
        self.expl_anim = {}
        expl_anim = self.expl_anim
        expl_anim['lg'] = []
        expl_anim['sm'] = []
        expl_anim['player'] = []
        for i in range(9):
            self.expl_img = pygame.image.load(os.path.join("img", f"expl{i}.png")).convert()
            expl_img = self.expl_img
            expl_img.set_colorkey(parameter.BLACK)
            expl_anim['lg'].append(pygame.transform.scale(expl_img, (75, 75)))
            expl_anim['sm'].append(pygame.transform.scale(expl_img, (30, 30)))
            self.player_expl_img = pygame.image.load(os.path.join("img", f"player_expl{i}.png")).convert()
            self.player_expl_img.set_colorkey(parameter.BLACK)
            expl_anim['player'].append(self.player_expl_img)
        self.power_imgs = {'shield': pygame.image.load(os.path.join("img", "shield.png")).convert(),
                           'gun': pygame.image.load(os.path.join("img", "gun.png")).convert()}
        # 載入音樂
        self.shoot_sound = pygame.mixer.Sound(os.path.join("sound", "shoot.wav"))
        self.gun_sound = pygame.mixer.Sound(os.path.join("sound", "pow1.wav"))
        self.shield_sound = pygame.mixer.Sound(os.path.join("sound", "pow0.wav"))
        self.die_sound = pygame.mixer.Sound(os.path.join("sound", "rumble.ogg"))
        self.expl_sounds = [
            pygame.mixer.Sound(os.path.join("sound", "expl0.wav")),
            pygame.mixer.Sound(os.path.join("sound", "expl1.wav"))
        ]
        pygame.mixer.music.load(os.path.join("sound", "background.ogg"))
        pygame.mixer.music.play(-1)  # 播放次數 -1=無限次
        pygame.mixer.music.set_volume(0.4)

        # 載入文字
        # font_name = pygame.font.match_font('arial')  # 不支援中文
        self.font_name = os.path.join("font.ttf")
