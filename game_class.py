import random
import object as obj
from config import *
import abc
import parameter


class GameClass:
    def __init__(self, show_init=True):
        self.__config = Config()
        self.__is_running = True
        self.__is_show_init = show_init
        self.__death_expl = ""
        self.__radius_rate = 0.85 / 2

        self.__score = 0
        self.__all_sprites = pygame.sprite.Group()
        self.__rocks = pygame.sprite.Group()
        self.__bullets = pygame.sprite.Group()
        self.__powers = pygame.sprite.Group()
        self.__player = obj.Player(self.__config, self.__all_sprites, self.__bullets)

        self.__all_sprites.add(self.__player)
        for i in range(8):
            self.__add_new_rock()

    def start(self):
        while self.__is_running:
            if self.__is_show_init:
                self.__draw_init()
                if self.__is_quit():
                    break
                else:
                    self.__init__(False)
            self.__refresh_game()
        pygame.quit()

    def __add_new_rock(self):
        r = obj.Rock(self.__config)
        self.__all_sprites.add(r)
        self.__rocks.add(r)

    def __draw_init(self):
        self.__config.screen.blit(self.__config.background_img, (0, 0))  # blit:畫到畫面上
        self.__draw_text(parameter.str_game_name, 64, parameter.WIDTH / 2, parameter.HEIGHT / 4)
        self.__draw_text(parameter.str_guide_play, 22, parameter.WIDTH / 2, parameter.HEIGHT / 2)
        self.__draw_text(parameter.str_guide_start, 18, parameter.WIDTH / 2, parameter.HEIGHT * 3 / 4)
        pygame.display.update()

    def __draw_text(self, text, size, x, y):
        surf = self.__config.screen
        font = pygame.font.Font(self.__config.font_name, size)
        text_surface = font.render(text, True, parameter.WHITE)
        text_rect = text_surface.get_rect()
        text_rect.centerx = x
        text_rect.top = y
        surf.blit(text_surface, text_rect)

    def __is_quit(self):
        while True:
            self.__config.clock.tick(parameter.FPS)  # while一秒鐘之內最多只能執行 FPS 次
            # 取得輸入
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return True
                elif event.type == pygame.KEYUP:  # KEYUP: 鍵盤按下又鬆開後
                    return False

    def __refresh_game(self):
        self.__config.clock.tick(parameter.FPS)  # while一秒鐘之內最多只能執行 FPS 次
        self.__get_input()  # 取得輸入
        self.__all_sprites.update()  # 更新遊戲
        self.__collide_event()
        self.__show_screen()  # 畫面顯示

    def __get_input(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.__is_running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self.__player.shoot()

    def __collide_event(self):
        self.__collide_rock_bullet()
        self.__collide_player_rock()
        self.__get_power()

    # 判斷子彈與石頭是否碰撞, 有碰撞就刪掉, 並產生新的石頭 (回傳一個字典)
    def __collide_rock_bullet(self):
        hits = pygame.sprite.groupcollide(self.__rocks, self.__bullets, True, True)
        for hit in hits:
            random.choice(self.__config.expl_sounds).play()
            self.__score += int(hit.rect.width * self.__radius_rate)
            expl = obj.Explosion(hit.rect.center, 'lg', self.__config)
            self.__all_sprites.add(expl)
            if random.random() > 0.95:
                power = obj.Power(hit.rect.center, self.__config)
                self.__all_sprites.add(power)
                self.__powers.add(power)
            self.__add_new_rock()

    # 判斷飛船與石頭是否碰撞; pygame.sprite.collide_circle:圓形碰撞
    def __collide_player_rock(self):
        hits = pygame.sprite.spritecollide(self.__player, self.__rocks, True, pygame.sprite.collide_circle)
        for hit in hits:
            self.__add_new_rock()
            self.__player.health -= int(hit.rect.width * self.__radius_rate)
            expl = obj.Explosion(hit.rect.center, 'sm', self.__config)
            self.__all_sprites.add(expl)
            if self.__player.health <= 0:
                self.__death_expl = obj.Explosion(self.__player.rect.center, 'player', self.__config)
                self.__all_sprites.add(self.__death_expl)
                self.__config.die_sound.play()
                self.__player.lives -= 1
                self.__player.health = 100
                self.__player.hide()  # 隱藏一段時間
        if self.__player.lives == 0 and not (self.__death_expl.alive()):
            self.__is_show_init = True

    # 判斷寶物跟飛船相撞
    def __get_power(self):
        hits = pygame.sprite.spritecollide(self.__player, self.__powers, True)
        for hit in hits:
            prop_effect = PropContext(hit.type, self.__player, self.__config)
            prop_effect.get_result()

    def __show_screen(self):
        self.__config.screen.fill(parameter.BLACK)
        self.__config.screen.blit(self.__config.background_img, (0, 0))  # blit:畫到畫面上
        self.__all_sprites.draw(self.__config.screen)
        self.__draw_text(str(self.__score), 18, parameter.WIDTH / 2, 10)
        self.__draw_health(self.__player.health, 5, 15)
        self.__draw_lives(self.__player.lives, self.__config.player_mini_img, parameter.WIDTH - 100, 15)
        pygame.display.update()

    def __draw_lives(self, lives, img, x, y):
        surf = self.__config.screen
        for index in range(lives):
            img_rect = img.get_rect()
            img_rect.x = x + 33 * index
            img_rect.y = y
            surf.blit(img, img_rect)

    def __draw_health(self, hp, x, y):
        surf = self.__config.screen
        if hp < 0:
            hp = 0
        fill = (hp / 100) * parameter.BAR_LENGTH
        outline_rect = pygame.Rect(x, y, parameter.BAR_LENGTH, parameter.BAR_HEIGHT)
        fill_rect = pygame.Rect(x, y, fill, parameter.BAR_HEIGHT)
        pygame.draw.rect(surf, parameter.GREEN, fill_rect)
        pygame.draw.rect(surf, parameter.WHITE, outline_rect, 2)  # 外框←


@abc.abstractmethod
class Prop:
    def __init__(self, player, config):
        self._player = player
        self._config = config

    def trigger_effect(self):
        pass


class EnhanceCapability(Prop):
    def __init__(self, player, config):
        super().__init__(player, config)

    def trigger_effect(self):
        self._player.gun_up()
        self._config.gun_sound.play()


class Cure(Prop):
    def __init__(self, player, config):
        super().__init__(player, config)

    def trigger_effect(self):
        self._player.health += 20
        if self._player.health > 100:
            self._player.health = 100
        self._config.shield_sound.play()


class PropContext:
    def __init__(self, type, player, config):
        self.__prop = Prop
        if type == 'shield':
            self.__prop = Cure(player, config)
        elif type == 'gun':
            self.__prop = EnhanceCapability(player, config)
        else:
            pass

    def get_result(self):
        return self.__prop.trigger_effect()
