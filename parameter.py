str_game_name = '太空生存戰'
str_guide_play = '← → 移動飛船  空白鍵發射子彈~'
str_guide_start = '按任意鍵開始遊戲'

FPS = 60
WIDTH = 500
HEIGHT = 600

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

BAR_LENGTH = 100
BAR_HEIGHT = 10
